import 'package:flutter/material.dart';
import '../example_data.dart';
import './card_item.dart';

class CardList extends StatelessWidget {
  const CardList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: posts.map<Widget>((post) {
        return CardItem(post: post);
      }).toList(),
    );
  }
}
