import 'package:flutter/material.dart';
import './card_list.dart';
import './search_bar.dart';
import '../example_data.dart';
import './post_form.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({Key? key}) : super(key: key);

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              showSearch(
                context: context,
                delegate: SearchPost(posts),
              );
            },
            icon: const Icon(Icons.search),
          ),
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PostForm()),
              );
            },
            icon: const Icon(Icons.add_sharp),
          ),
        ],
      ),
      body: const Center(
        child: CardList(),
      ),
    );
  }
}
