from django.urls import path, include
from .views import index, xml, json
import lab_2.urls as lab_2

urlpatterns = [
    path('', index, name='index'),
    path('xml/', xml, name='xml'),
    path('json/', json, name='json')
]