# Pertanyaan dan Jawaban Lab 2

## Apakah perbedaan antara JSON dan XML?

JSON (_JavaScript Object Notation_) adalah sebuah format pertukaran data yang ringan berdasarkan JavaScript. JSON sama sekali tidak bergantung kepada bahasa pemrograman apa pun. JSON menyimpan semua datanya dalam format map/dictionary (pasangan key dan value) yang mudah untuk dibaca.

Di sisi lain, XML (_eXtensible Markup Language_) adalah sebuah bahasa markup berdasarkan SGML yang dirancang untuk mengangkut dokumen dalam format yang dapat dibaca oleh mesin dan manusia. Hampir semua bahasa pemrograman di zaman sekarang memiliki parser untuk format XML. Dokumen XML disimpan sebagai sebuah struktur data tree, menggunakan tags untuk mendefinisikan elemen-elemen di dalamnya.

Pada dasarnya, keduanya merupakan sebuah syntax yang dapat digunakan untuk menyimpan dan menukar data. Keduanya dapat diparse menggunakan berbagai macam bahasa pemrograman. Perbedaan utama di antara keduanya adalah syntaxnya: JSON menggunakan kurung kurawal untuk mendefinisikan objek-objek, sedangkan XML menggunakan struktur tag untuk mendefinisikan elemen-elemen. Oleh sebab ini, JSON mungkin lebih mudah untuk dibaca dan dipahami dibandingkan XML. Struktur tag XML membuat filenya cenderung lebih panjang dibandingkan sebuah file JSON.

XML dapat memproses dan memformat dokumen-dokumennya, sedangkan JSON tidak melakukan pemrosesan. Sebagai contoh, XML sanggup memvalidasi dokumen-dokumen dan schema yang mendefinisikan struktur dari sebuah dokumen XML, membuatnya lebih aman daripada JSON. XML juga dapat menambahkan metadata seperti atribut-atribut dan namespace untuk elemen-elemennya. JSON tidak memiliki ketentuan spesifik untuk fitur-fitur tersebut, namun ini membuat JSON lebih cepat untuk diparse dan dikirim daripada XML.

Perbedaan lainnya adalah tipe-tipe data yang didukung oleh XML dan JSON. JSON hanya mendukung tipe-tipe data primitif, seperti string, integer, boolean, dan array. JSON juga mengandung objek-objek, namun hanya dapat berisi tipe-tipe data primitif yang telah disebutkan. Di samping itu, XML mendukung tipe-tipe data yang lebih rumit, seperti citra, grafik, dan lain-lain. Namun, XML tidak mendukung tipe data array secara langsung seperti JSON.

Jadi, sebagai sebuah kesimpulan, keduanya dapat digunakan untuk melayani tujuan yang mirip. Jika dibutuhkan validasi data atau metadata, gunakan XML. Jika tidak, gunakan JSON untuk pengiriman data yang lebih cepat secara umum.

## Apakah perbedaan antara HTML dan XML?

Tidak seperti XML, HTML (_HyperText Markup Language_) digunakan sebagai bahasa markup standar untuk menampilkan data di web browser kesayangan Anda, bukan untuk mengangkutnya. Seperti XML, HTML menggunakan struktur tag juga untuk mendefinisikan elemen-elemen dari tampilan halaman webnya, tetapi tag di dalam HTML sudah memiliki nama-nama terbatas yang telah ditentukan dan tidak dapat didefinisikan sendiri oleh user. Ini berarti HTML juga memiliki struktur data serupa tree seperti XML. Kegunaan utama dari HTML adalah untuk menampilkan hypertext yang menghubungkan sumber-sumber yang berada dalam web. Dibandingkan dengan XML, HTML bersifat statis, sedangkan XML dapat bersifat dinamis.
