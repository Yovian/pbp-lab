from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from datetime import datetime, date
from lab_2.models import Note
from .forms import NoteForm

def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if request.method == 'POST' and form.is_valid:
        form.save()
        return HttpResponseRedirect('/lab-4')
    context = {
        'form': form
    }
    return render(request, 'lab4_form.html', context)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)