from django.urls import path, include
from .views import index, add_note, note_list
import lab_4.urls as lab_4

urlpatterns = [
    path('', index, name='index'),
    path('add-note/', add_note, name='add-note'),
    path('note-list/', note_list, name='note-list')
]