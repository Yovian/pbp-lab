from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['recipient', 'sender', 'title', 'message']
        labels = {
            'recipient': '',
            'sender': '',
            'title': '',
            'message': ''
        }
        widgets = {
            'recipient': forms.TextInput(attrs={'placeholder': 'Recipient'}),
            'sender': forms.TextInput(attrs={'placeholder': 'Sender'}),
            'title': forms.TextInput(attrs={'placeholder': 'Note title'}),
            'message': forms.Textarea(attrs={'placeholder': 'Write your note here.', 'cols': 50, 'rows': 16})
        }