from django.urls import path, include
from .views import index, friend_list
import lab_1.urls as lab_1

urlpatterns = [
    path('', index, name='index'),
    path('friends/', friend_list, name='friend_list'),
    #path('lab-1/', include(lab_1))
]
