import 'package:flutter/material.dart';
import '../example_data.dart';
import 'card_item.dart';

class CardList extends StatelessWidget {
  const CardList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          for (final post in posts)
            CardItem(
              title: post.title,
              subtitle: post.subtitle,
              thumbnailUrl: post.thumbnailUrl,
              bodyText: post.bodyText,
              datePublished: post.datePublished,
              author: post.author,
            ),
        ],
      ),
    );
  }
}
