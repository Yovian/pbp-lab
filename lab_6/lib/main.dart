import 'package:flutter/material.dart';
import 'package:lab_6/widgets/card_list.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bloggers',
      theme: ThemeData(
        primaryColor: Colors.amber,
        canvasColor: const Color(0xFF0F0F17),
        textTheme: const TextTheme(
          headline4: TextStyle(
            color: Color(0xFFF2F8F2),
            fontSize: 30,
            fontFamily: 'Playfair Display',
            fontWeight: FontWeight.w400,
          ),
          headline5: TextStyle(
            color: Color(0xFFE6F8F2),
            fontSize: 18,
            fontFamily: 'Lato',
            fontWeight: FontWeight.w300,
            fontStyle: FontStyle.italic,
          ),
          bodyText1: TextStyle(
            color: Color(0xFFF2F8F2),
            fontSize: 16,
            fontFamily: 'PT Serif',
            fontWeight: FontWeight.normal,
          ),
          caption: TextStyle(
            color: Color(0xC0E6F8F2),
            fontSize: 12,
            fontFamily: 'Lato',
            fontWeight: FontWeight.normal,
          ),
        ),
        primarySwatch: Colors.blue,
      ),
      home: const Scaffold(
        body: Center(
          child: CardList(),
        ),
      ),
    );
  }
}
