class Post {
  const Post({
    required this.title,
    required this.subtitle,
    required this.thumbnailUrl,
    required this.bodyText,
    required this.datePublished,
    required this.author,
  });

  final String title;
  final String subtitle;
  final String thumbnailUrl;
  final String bodyText;
  final String datePublished;
  final String author;
}
