import './models/post.dart';

const prefixUrl = 'assets/images';

const posts = [
  Post(
      title: 'This is Blog Post Number One',
      subtitle: 'Welcome Flutter!',
      thumbnailUrl: '$prefixUrl/blog_one.jpg',
      bodyText: 'Lorem ipsum.',
      datePublished: 'November 16',
      author: 'Stefanus'),
  Post(
      title: 'The Great Second Blog Post',
      subtitle: 'A subtitle for this post, a great one.',
      thumbnailUrl: '$prefixUrl/blog_two.jpg',
      bodyText: 'Ipsum lorem.',
      datePublished: 'August 16',
      author: 'Stefanus'),
];
