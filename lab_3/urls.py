from django.urls import path, include
from .views import index, add_friend
import lab_3.urls as lab_3

urlpatterns = [
    path('', index, name='index'),
    path('add/', add_friend, name='add')
]