from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'birth_date']
        labels = {
            'name': 'Name',
            'npm': 'NPM',
            'birth_date': 'Date of Birth'
        }
    name = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Enter the student\'s name.',  'style': 'width: 200px'}))
    npm = forms.IntegerField(min_value=1000000000, max_value=9999999999, widget=forms.NumberInput(attrs={'placeholder': 'Enter the student\'s NPM.', 'style': 'width: 200px'}))
    birth_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))